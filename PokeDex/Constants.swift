//
//  Constants.swift
//  PokeDex
//
//  Created by Paul Lascar on 09/08/2017.
//  Copyright © 2017 Paul Lascar. All rights reserved.
//

import Foundation


let BASE_URL = "http://pokeapi.co"
let URL_POKEMON = "/api/v1/pokemon/"

typealias DownloadComplete = () -> ()
